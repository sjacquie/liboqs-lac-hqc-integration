// SPDX-License-Identifier: MIT

#ifndef OQS_KEM_lac_hqc
#define OQS_KEM_lac_hqc

#include <oqs/oqs.h>

#define OQS_KEM_lac_hqc_length_public_key 3125
#define OQS_KEM_lac_hqc_length_secret_key 3165
#define OQS_KEM_lac_hqc_length_ciphertext 6234
#define OQS_KEM_lac_hqc_length_shared_secret 64

#ifndef OQS_KEM_alg_lac_hqc
#define OQS_KEM_alg_lac_hqc "LAC_HQC"
#endif

OQS_KEM *OQS_KEM_lac_hqc_new(void);
OQS_API OQS_STATUS OQS_KEM_lac_hqc_keypair(uint8_t *public_key, uint8_t *secret_key);
OQS_API OQS_STATUS OQS_KEM_lac_hqc_encaps(uint8_t *ciphertext, uint8_t *shared_secret, const uint8_t *public_key);
OQS_API OQS_STATUS OQS_KEM_lac_hqc_decaps(uint8_t *shared_secret, const unsigned char *ciphertext, const uint8_t *secret_key);

#endif

