// SPDX-License-Identifier: MIT

#include <stdlib.h>
#include <stdio.h>

#include "kem_lac_hqc.h"
#include "src/api.h"

OQS_KEM *OQS_KEM_lac_hqc_new() {

    OQS_KEM *kem = malloc(sizeof(OQS_KEM));
    if (kem == NULL) {
        return NULL;
    }
    kem->method_name = OQS_KEM_alg_lac_hqc;
    kem->alg_version = "LASEC";

    kem->claimed_nist_level = 1;
    kem->ind_cca = true;

    kem->length_public_key = OQS_KEM_lac_hqc_length_public_key;
    kem->length_secret_key = OQS_KEM_lac_hqc_length_secret_key;
    kem->length_ciphertext = OQS_KEM_lac_hqc_length_ciphertext;
    kem->length_shared_secret = OQS_KEM_lac_hqc_length_shared_secret;

    kem->keypair = OQS_KEM_lac_hqc_keypair;
    kem->encaps = OQS_KEM_lac_hqc_encaps;
    kem->decaps = OQS_KEM_lac_hqc_decaps;

    return kem;
}

OQS_API OQS_STATUS OQS_KEM_lac_hqc_keypair(uint8_t *public_key, uint8_t *secret_key) {
    int res;
    OQS_STATUS ret;
    res = crypto_kem_keypair((unsigned char *) public_key, (unsigned char *) secret_key);
    if (res == 0) {
        ret = OQS_SUCCESS;
    } else {
        printf("OQS KEM LAC HQC KEYPAIR RETURN : %i", res);
        ret = OQS_ERROR;
    }
    return ret;
}

OQS_API OQS_STATUS OQS_KEM_lac_hqc_encaps(uint8_t *ciphertext, uint8_t *shared_secret, const uint8_t *public_key) {
    int res;
    OQS_STATUS ret;
    res = crypto_kem_enc((unsigned char *) ciphertext, (unsigned char *) shared_secret, (unsigned char *) public_key);
    if (res == 0) {
        ret = OQS_SUCCESS;
    } else {
        printf("OQS KEM LAC HQC ENCAPS RETURN : %i", res);
        ret = OQS_ERROR;
    }
    return ret;
}

OQS_API OQS_STATUS OQS_KEM_lac_hqc_decaps(uint8_t *shared_secret, const unsigned char *ciphertext, const uint8_t *secret_key) {
    int res;
    OQS_STATUS ret;
    res = crypto_kem_dec((unsigned char *) shared_secret, (unsigned char *) ciphertext, (unsigned char *) secret_key);
    if (res == 0) {
        ret = OQS_SUCCESS;
    } else {
        printf("OQS KEM LAC HQC DECAPS RETURN : %i", res);
        ret = OQS_ERROR;
    }
    return ret;
}
