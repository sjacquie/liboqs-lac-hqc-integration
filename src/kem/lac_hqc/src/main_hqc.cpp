#include <time.h>
#include <sys/time.h>
#include <pthread.h>
#include <stdio.h>
#include "hqc.h"
#include "api.h"
#include "parameters.h"
#include "vector.h"
extern "C" {
 #include "api_lac.h"
 #include "rand.h"
}
#define NTESTS 10000.0

struct key_pair_t {
  unsigned char *pk;
  unsigned char *sk;
};

struct public_key_inst_t {
  const unsigned char *pk;
  unsigned char *sigma;
  unsigned char *seed;
  unsigned char *c;
};

struct secret_key_inst_t {
  const unsigned char *sk;
  const unsigned char *c;
  unsigned char *sigma;
};

struct lac_setup_t {
  unsigned char *sigma;
  unsigned char *seed;
};

// Not used anymore
void *setup_hqc_thread(void *args){
  struct lac_setup_t* s = (struct lac_setup_t *) args;
  vect_set_random_from_randombytes(s->sigma);
  gen_seed(s->sigma, MESSAGE_LEN, s->seed);
  pthread_exit(NULL);
  return NULL;
}
//Not used anymore
void *setup_lac_thread(void *args){
  struct lac_setup_t* s = (struct lac_setup_t *) args;
  // generate sigma_1
  random_bytes(s->sigma,MESSAGE_LEN);
  //compute seed = H(sigma_1)
  gen_seed(s->sigma, MESSAGE_LEN, s->seed);
  pthread_exit(NULL);
  return NULL;
}

// Function for the lac encryption thread
void *encrypt_lac_thread(void *args){
  unsigned long long clen;
  unsigned char theta_1[SEED_LEN];
  struct public_key_inst_t* s = (struct public_key_inst_t *) args;
  gen_seed(s->seed, 2*MESSAGE_LEN, theta_1);
  pke_enc_seed(s->pk,s->sigma,MESSAGE_LEN,s->c,&clen,theta_1); 
  pthread_exit(NULL);

  return NULL;
}

// Function for the HQC encryption thread
void *encrypt_hqc_thread(void *args){
  unsigned char theta_2[SEED_BYTES];
  unsigned char u[VEC_N_SIZE_BYTES] = {0};
  unsigned char v[VEC_N1N2_SIZE_BYTES] = {0};  
  struct public_key_inst_t* s = (struct public_key_inst_t *) args;
  // Generating G function
  unsigned char diversifier_bytes[8];
  for(int i = 0; i < 8; ++i) {
    diversifier_bytes[i] = 0;
  }
  AES_XOF_struct* G_seedexpander = (AES_XOF_struct*) malloc(sizeof(AES_XOF_struct));
  seedexpander_init(G_seedexpander, s->seed, diversifier_bytes, SEEDEXPANDER_MAX_LENGTH);
  // Computing theta_2
  seedexpander(G_seedexpander, theta_2, SEED_BYTES);
  hqc_pke_encrypt(u, v, s->sigma, theta_2, s->pk);
  memcpy(s->c, u, VEC_N_SIZE_BYTES);
  memcpy(s->c + VEC_N_SIZE_BYTES, v, VEC_N1N2_SIZE_BYTES);
  pthread_exit(NULL);

  return NULL;
}

// Function for the LAC keygen thread
void *gen_key_lac_thread(void *args){
  struct key_pair_t* kp = (struct key_pair_t*) args;
  crypto_encrypt_keypair(kp->pk,kp->sk);
  //hqc_pke_keygen(kp->pk,kp->sk);
  pthread_exit(NULL);
  return NULL;
}
// Function for the HQC keygen thread
void *gen_key_hqc_thread(void *args){
  struct key_pair_t* kp = (struct key_pair_t*) args;
  //crypto_encrypt_keypair(kp->pk,kp->sk);
  hqc_pke_keygen(kp->pk,kp->sk);
  pthread_exit(NULL);
  return NULL;
}

// Function for the HQC decryption thread
void *decrypt_hqc_thread(void *args){
  unsigned char u[VEC_N_SIZE_BYTES] = {0};
  unsigned char v[VEC_N1N2_SIZE_BYTES] = {0};  
  struct secret_key_inst_t* s = (struct secret_key_inst_t *) args;
  memcpy(u, s->c, VEC_N_SIZE_BYTES);
  memcpy(v, s->c + VEC_N_SIZE_BYTES, VEC_N1N2_SIZE_BYTES);
  hqc_pke_decrypt(s->sigma,u,v,s->sk);
  pthread_exit(NULL);

  return NULL;
}
// Function for the LAC decryption thread
void *decrypt_lac_thread(void *args){
  unsigned long long mlen;
  struct secret_key_inst_t* s = (struct secret_key_inst_t *) args;
  pke_dec(s->sk,s->c,CIPHER_LEN,s->sigma,&mlen);
  pthread_exit(NULL);

  return NULL;
}

// KeyGen function of lac_hqc
// call lac key_gen in another thread for speedup
int hybrid_key_pair(unsigned char *pk_1, unsigned char *pk_2, unsigned char *sk_1, unsigned char *sk_2)
{
  int rc_1;
  pthread_t key_gen;
  struct key_pair_t kp_t = {.pk = pk_1, .sk = sk_1};
  rc_1 = pthread_create(&key_gen, NULL, gen_key_lac_thread, (void *)&kp_t);
  //crypto_encrypt_keypair(pk_1,sk_1);
  hqc_pke_keygen(pk_2,sk_2);
  pthread_join(key_gen,NULL);

  return 0;
}

// Decryption function of lac_hqc
// call LAC decryption function in another thread 
int hybrid_kem_dec(const unsigned char *pk_1, const unsigned char *pk_2, const unsigned char *sk_1, const unsigned char *sk_2, const unsigned char *c_1, const unsigned char *c_2, unsigned char *k)
{
  unsigned char sigma1_dec[MESSAGE_LEN], sigma2_dec[MESSAGE_LEN], sigma_buf[MESSAGE_LEN], seed[SEED_LEN+1];
  unsigned char buff[2*MESSAGE_LEN];
  unsigned char theta_2[SEED_BYTES];
  unsigned char c1_v[CIPHER_LEN];
  //unsigned long long mlen,clen;
  unsigned char u[VEC_N_SIZE_BYTES] = {0};
  unsigned char v[VEC_N1N2_SIZE_BYTES] = {0};  
  int rc_1;
  pthread_t dec_1; 

  // Comment/Uncomment next 4 lines to call HQC or LAC decryption in another thread
  secret_key_inst_t args_1 = {.sk = sk_1, .c = c_1, .sigma = sigma1_dec};
  //secret_key_inst_t args_1 = {.sk = sk_2, .c = c_2, .sigma = sigma2_dec};
  rc_1 = pthread_create(&dec_1, NULL, decrypt_lac_thread, (void *)&args_1);
  //rc_1 = pthread_create(&dec_1, NULL, decrypt_hqc_thread, (void *)&args_1);
  
  memcpy(u, c_2, VEC_N_SIZE_BYTES);
  memcpy(v, c_2 + VEC_N_SIZE_BYTES, VEC_N1N2_SIZE_BYTES);
  hqc_pke_decrypt(sigma2_dec,u,v,sk_2); 

  pthread_join(dec_1, NULL);
  memcpy(buff, sigma1_dec, MESSAGE_LEN);
  memcpy(buff+MESSAGE_LEN, sigma2_dec, MESSAGE_LEN);
  
  //compute theta_1 = SHA(seed)
  //gen_seed(seed, SEED_LEN, theta_1);
  // Generating G function
  unsigned char diversifier_bytes[8];
  for(int i = 0; i < 8; ++i) {
    diversifier_bytes[i] = 0;
  }
  AES_XOF_struct* G_seedexpander = (AES_XOF_struct*) malloc(sizeof(AES_XOF_struct));
  seedexpander_init(G_seedexpander, buff, diversifier_bytes, SEEDEXPANDER_MAX_LENGTH);
  // Computing theta_2
  seedexpander(G_seedexpander, theta_2, SEED_BYTES);
  // Reencryption
  seed[SEED_LEN] = 0x2;
  struct public_key_inst_t pi_1 = {.pk = pk_1, .sigma = sigma1_dec, .seed = buff, .c = c1_v};
  //pthread_t enc_1;
  rc_1 = pthread_create(&dec_1, NULL, encrypt_lac_thread, (void *)&pi_1);
  //pke_enc_seed(pk_1,sigma1_dec,MESSAGE_LEN,c1_v,&clen,theta_1); 
  unsigned char u_v[VEC_N_SIZE_BYTES] = {0};
  unsigned char v_v[VEC_N1N2_SIZE_BYTES] = {0};  
  hqc_pke_encrypt(u_v, v_v, sigma2_dec, theta_2, pk_2);

  pthread_join(dec_1, NULL);

  // Check c = c'
  int abort = 0;
  if(memcmp(c_1,c1_v,CIPHER_LEN)!= 0) abort = 1;
  if(vect_compare(u, u_v, VEC_N_SIZE_BYTES) != 0) abort = 1;
  if(vect_compare(v, v_v, VEC_N1N2_SIZE_BYTES) != 0) abort = 1;
  //printf("\n ABORT = %d \n", abort);
  for(int i = 0; i < MESSAGE_LEN; ++i)
    sigma_buf[i] = sigma1_dec[i] ^ sigma2_dec[i];
  sha512(k,sigma_buf,MESSAGE_LEN);
  /*
  printf("\n Shared key at decaps \n");
  for(int i = 0; i < SHARED_SECRET_BYTES; ++i)
    printf("%02x",k[i]);
  printf("\n\n"); */
  if(abort){
    memset(k, 0, SHARED_SECRET_BYTES);
    return -1;
  }
  return 0;
}

// Encryption function of lac_hqc
// call LAC encryption function in another thread 
int hybrid_kem_enc(const unsigned char *pk_1, const unsigned char *pk_2, unsigned char *k, unsigned char *c_1, unsigned char *c_2){
  unsigned char sigma_1[MESSAGE_LEN], sigma_2[MESSAGE_LEN] = {0}, sigma_buf[MESSAGE_LEN], seed[SEED_LEN+1];
  unsigned char buff[2*MESSAGE_LEN];
  unsigned char theta_2[SEED_BYTES];
  pthread_t enc_1;
  int rc_1;
  //lac_setup_t setup_1 = {.sigma = sigma_1, .seed = seed};
  //rc_1 = pthread_create(&enc_1, NULL, setup_lac_thread, (void *)&setup_1);
  //rc_1 = pthread_create(&enc_1, NULL, setup_hqc_thread, (void *)&setup_1);
  
  // generate sigma_1
  random_bytes(sigma_1,MESSAGE_LEN);
  //compute seed = H(sigma_1)
  
  // generate sigma_2
  vect_set_random_from_randombytes(sigma_2);
  memcpy(buff, sigma_1, MESSAGE_LEN);
  memcpy(buff+MESSAGE_LEN, sigma_2, MESSAGE_LEN);
  
  seed[SEED_LEN] = 0x2;
  struct public_key_inst_t pi_1 = {.pk = pk_1, .sigma = sigma_1, .seed = buff, .c = c_1};
  //pthread_t enc_1;
  rc_1 = pthread_create(&enc_1, NULL, encrypt_lac_thread, (void *)&pi_1);

  
  unsigned char u[VEC_N_SIZE_BYTES] = {0};
  unsigned char v[VEC_N1N2_SIZE_BYTES] = {0};  
  hqc_pke_encrypt(u, v, sigma_2, theta_2, pk_2);
  memcpy(c_2, u, VEC_N_SIZE_BYTES);
  memcpy(c_2 + VEC_N_SIZE_BYTES, v, VEC_N1N2_SIZE_BYTES);
  
  pthread_join(enc_1,NULL);
  //pthread_join(enc_2,NULL);

  for(int i = 0; i < MESSAGE_LEN; ++i)
    sigma_buf[i] = sigma_1[i] ^ sigma_2[i];
  sha512(k,sigma_buf,MESSAGE_LEN);
  /*printf("\n Shared key at encaps \n");
  for(int i = 0; i < SHARED_SECRET_BYTES; ++i)
    printf("%02x",k[i]);*/

  return 0;
}

// Benchmarking function 
int test_kem_fo_speed()
{
  //clock_t start,finish;
  //double total_time;
  unsigned char pk_1[LAC_CRYPTO_PUBLICKEYBYTES];
  unsigned char sk_1[LAC_CRYPTO_SECRETKEYBYTES];
  unsigned char pk_2[HQC_CRYPTO_PUBLICKEYBYTES];
  unsigned char sk_2[HQC_CRYPTO_SECRETKEYBYTES];
  unsigned char ct_1[CIPHER_LEN];
  unsigned char ct_2[VEC_N_SIZE_BYTES+VEC_N1N2_SIZE_BYTES];
  unsigned char key1[SHARED_SECRET_BYTES];
  unsigned char key2[SHARED_SECRET_BYTES];
  int i;
  struct timeval begin, end;

  //start=clock();
  gettimeofday(&begin, NULL);
  for(i=0;i<NTESTS;i++)
  {
    hybrid_key_pair(pk_1, pk_2, sk_1, sk_2);
  }
  gettimeofday(&end, NULL);
  //finish=clock();
  //total_time=(double)(finish-start)/CLOCKS_PER_SEC;
  double elapsed = ((end.tv_sec - begin.tv_sec)*1000000 + (end.tv_usec - begin.tv_usec)) / NTESTS;
  printf("key generate time mt  :%f us\n",elapsed);
  //total_time=(double)(finish-start)/CLOCKS_PER_SEC;
  //printf("key generate time:%f us\n",(total_time*1000000/NTESTS));

  hybrid_key_pair(pk_1, pk_2, sk_1, sk_2);

  //start=clock();
  gettimeofday(&begin, NULL);
  for(i=0;i<NTESTS;i++)
  {
    hybrid_kem_enc(pk_1,pk_2,key1,ct_1,ct_2);
  }
  gettimeofday(&end, NULL);
  //finish=clock();
  //total_time=(double)(finish-start)/CLOCKS_PER_SEC;
  elapsed = ((end.tv_sec - begin.tv_sec)*1000000 + (end.tv_usec - begin.tv_usec)) / NTESTS;
  printf("kem_fo_enc time mt  :%f us\n",elapsed);
  //printf("kem_fo_enc time  :%f us\n",(total_time*1000000/NTESTS));

  hybrid_key_pair(pk_1, pk_2, sk_1, sk_2);
  hybrid_kem_enc(pk_1,pk_2,key1,ct_1,ct_2);

  //start=clock();
  gettimeofday(&begin, NULL);
  for(i=0;i<NTESTS;i++)
  {
    hybrid_kem_dec(pk_1,pk_2,sk_1,sk_2,ct_1,ct_2,key2);
  }
  gettimeofday(&end, NULL);
  //finish=clock();
  elapsed = ((end.tv_sec - begin.tv_sec)*1000000 + (end.tv_usec - begin.tv_usec)) / NTESTS;
  printf("kem_fo_dec time mt  :%f us\n",elapsed);
  //total_time=(double)(finish-start)/CLOCKS_PER_SEC;
  //printf("kem_fo_dec time  :%f us\n",(total_time*1000000/NTESTS));
  printf("\n");

  return 0;
}
int main() {

  printf("\n");
  printf("********************\n");
  printf("**** LAC_HQC-%d-%d ****\n", PARAM_SECURITY, PARAM_DFR_EXP);
  printf("********************\n");

  printf("\n");
  printf("N: %d   ", PARAM_N);
  printf("N1: %d   ", PARAM_N1);
  printf("N2: %d   ", PARAM_N2);
  printf("OMEGA: %d   ", PARAM_OMEGA);
  printf("OMEGA_R: %d   ", PARAM_OMEGA_R);
  printf("Failure rate: 2^-%d   ", PARAM_DFR_EXP);
  printf("Sec: %d bits", PARAM_SECURITY);
  printf("\n");

  /*
  unsigned char pk_1[LAC_CRYPTO_PUBLICKEYBYTES];
  unsigned char sk_1[LAC_CRYPTO_SECRETKEYBYTES];
  unsigned char pk_2[HQC_CRYPTO_PUBLICKEYBYTES];
  unsigned char sk_2[HQC_CRYPTO_SECRETKEYBYTES];
  unsigned char ct_1[CIPHER_LEN];
  unsigned char ct_2[VEC_N_SIZE_BYTES+VEC_N1N2_SIZE_BYTES];
  unsigned char key1[SHARED_SECRET_BYTES];
  unsigned char key2[SHARED_SECRET_BYTES];*/
  test_kem_fo_speed();
  return 0;
  /*
  hybrid_key_pair(pk_1, pk_2, sk_1, sk_2);
  for(size_t i = 0; i < LAC_CRYPTO_SECRETKEYBYTES; ++i) printf("%x",sk_1[i]);
  printf("\n\n");
  hybrid_key_pair(pk_1, pk_2, sk_1, sk_2);
  for(size_t i = 0; i < LAC_CRYPTO_SECRETKEYBYTES; ++i) printf("%x",sk_1[i]);
  printf("\n ======== HQC SK ========== \n");
  //for(size_t i = 0; i < HQC_CRYPTO_SECRETKEYBYTES; ++i) printf("%x",sk_2[i]);
  hybrid_kem_enc(pk_1,pk_2,key1,ct_1,ct_2);
  printf("\n ===== Decapsulation ==== \n");
  //ct_2[2] += 0x01;
  hybrid_kem_dec(pk_1,pk_2,sk_1,sk_2,ct_1,ct_2,key2);
  for(size_t i = 0; i < CIPHER_LEN; ++i)
    printf("%x", ct_1[i]);*/
  /*
     crypto_kem_keypair(pk, sk);
     crypto_kem_enc(ct, key1, pk);
     crypto_kem_dec(key2, ct, sk);

     printf("\n\nsecret1: ");
     for(int i = 0 ; i < SHARED_SECRET_BYTES ; ++i) printf("%x", key1[i]);

     printf("\nsecret2: ");
     for(int i = 0 ; i < SHARED_SECRET_BYTES ; ++i) printf("%x", key2[i]);
     printf("\n\n"); */

}
